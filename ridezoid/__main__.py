
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import argparse
import glob
import os
import sys
import logging
import time
from pathlib import Path

from fontTools.ttLib import TTFont
from fontTools.unicode import Unicode
from PIL import Image, ImageDraw, ImageFont
from rgbmatrix import RGBMatrix, RGBMatrixOptions

parser = argparse.ArgumentParser()


def _convertImageForLEDLibrary(source_image, source_width, source_height):
    destination = Image.new('RGB', (source_width, source_height))
    source_pixels = source_image.load()
    destination_pixels = destination.load()
    col = 0
    for width in range(source_width):
        if width in [12, 28, 44]:
            continue

        for height in range(source_height):
            if width > 56:
                pixel = (0, 0, 0)
            else:
                pixel = source_pixels[col, height]
            destination_pixels[width, height] = pixel

        col += 1
    logger.debug('Image converted for LED library')
    return destination


def _fontHasCharacter(font_path: Path, character: str) -> bool:
    font = TTFont(font_path)
    for table in font['cmap'].tables:
        if ord(character) in table.cmap.keys():
            return True
    return False


def findFont(message: str) -> Path:
    font_paths = [Path('resources/fonts')]
    fonts = []
    for font_path in font_paths:
        if font_path.exists():
            fonts.extend(font_path.glob('NotoSans*Regular.*'))

    for font_path in fonts:
        if all((_fontHasCharacter(font_path, char) for char in message)):
            return font_path
    raise Exception('Could not find any suitable fonts')


def msleep(value):
    time.sleep(value / 1000.0)


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(1)
    stream = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('[%(asctime)s - %(name)s - %(levelname)s] - %(message)s')
    stream.setFormatter(formatter)
    stream.setLevel(logging.INFO)
    logger.addHandler(stream)

    logging.getLogger('PIL.Image').setLevel(logging.CRITICAL)
    logging.getLogger('fontTools.ttLib.ttFont').setLevel(logging.CRITICAL)

    parser.add_argument('image')
    parser.add_argument('--brightness', type=int, default=25)
    parser.add_argument('--fade-in-ms', type=int, default=1000)
    parser.add_argument('--wait-ms', type=int, default=3000)
    parser.add_argument('--fade-out-ms', type=int, default=1000)
    parser.add_argument('--status', default='')
    parser.add_argument('--name', default='')
    parser.add_argument('--test', default='test.png')
    parser.add_argument('-v', '--verbose', action='count', default=0)

    args = parser.parse_args()

    if args.test:
        args.test = Path(args.test)

    if args.verbose >= 1:
        stream.setLevel(logging.DEBUG)

    if not args.status:
        args.status = ''
    if not args.name:
        args.name = ''

    logger.info('Params: {}'.format(
        args.image,
        args.brightness,
        args.fade_in_ms,
        args.wait_ms,
        args.fade_out_ms,
        args.status,
        args.name))

    args.brightness = max(3, min(100, args.brightness))

    logger.debug('brightness that will be used: {}'.format(args.brightness))

    if args.name:
        args.name = args.name[0:10]

        fontfile = findFont(args.name)
        logger.debug('Chose font {}'.format(fontfile))

        # Work out which font size to fix all the text
        size = 25
        w = 100
        while (w > 54) and (size > 9):
            size -= 1
            font = ImageFont.truetype(str(fontfile), size)
            (w, h) = font.getsize(args.name)

        logger.debug('Font size: {}'.format(size))
        logger.debug('Font w: {}'.format(w))
        logger.debug('Font h: {}'.format(h))

        image = Image.new('RGB', (60, 30))
        draw = ImageDraw.Draw(image)
        x = (60 - w) / 2 - 3
        y = (30 - (h * 1.2)) / 2

        logger.debug('Image x, y: {},{}'.format(x, y))
        draw.text((x, y), args.name, (255, 255, 255), font=font)
    else:
        image = Image.open(args.image)

    if args.status:
        man_image = Image.open('resources/human.tga')
        image.paste(man_image, (0, 0), man_image)

    columns = 60
    rows = 30
    # Modify image as workaround for rendering bug in the LED library
    image = _convertImageForLEDLibrary(image, columns, rows)

    if args.test:
        image.save(args.test)
        logger.info('Test file written to {}'.format(args.test))
    else:
        os.system('modprobe w1-gpio')
        # Configuration for the matrix
        options = RGBMatrixOptions()
        options.rows = rows
        options.cols = columns
        options.chain_length = 1
        options.parallel = 1
        options.hardware_mapping = 'regular'

        matrix = RGBMatrix(options=options)

        max_brightness = args.brightness
        matrix.brightness = max_brightness

        logger.debug('max_brightness: {}'.format(max_brightness))

        brightness_change_per_step = max(1, min(3, round(max_brightness / 10)))
        steps_per_fade = max_brightness / brightness_change_per_step
        fade_in_ms_per_step = args.fade_in_ms / steps_per_fade
        fade_out_ms_per_step = args.fade_out_ms / steps_per_fade

        logger.debug('brightness_change_per_step: {}'.format(brightness_change_per_step))
        logger.debug('steps_per_fade: {}'.format(steps_per_fade))
        logger.debug('fade_in_ms: {}'.format(args.fade_in_ms))
        logger.debug('fade_in_ms_per_step: {}'.format(fade_in_ms_per_step))
        logger.debug('fade_out_ms_per_step: {}'.format(fade_out_ms_per_step))

        try:
            logger.info('Press CTRL-C to stop')
            matrix.brightness = 0

            while (matrix.brightness < max_brightness):
                matrix.brightness = min(max_brightness, matrix.brightness + brightness_change_per_step)
                matrix.SetImage(image.convert('RGB'))
                msleep(fade_in_ms_per_step)

            msleep(args.wait_ms)

            while (matrix.brightness > 0):
                matrix.brightness = max(0, matrix.brightness - brightness_change_per_step)
                matrix.SetImage(image.convert('RGB'))
                msleep(fade_out_ms_per_step)

        except KeyboardInterrupt:
            sys.exit(0)
