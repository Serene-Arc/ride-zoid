#!/usr/bin/env python
# -*- coding: utf-8 -*-

print("start")

import time
import sys

from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image, ImageFont, ImageDraw

print("import finished")

fontfile = "/usr/share/fonts/truetype/freefont/FreeMono.ttf"
fontfile_bold = "/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf"

#fontfile = "/usr/share/fonts/truetype/noto/NotoSans-Regular.ttf"
#fontfile_bold = "/usr/share/fonts/truetype/noto/NotoSans-Bold.ttf"

#fontfile = "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf"
#fontfile_bold = "/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf"

# --- TEMPERATURE READING CODE:

import os
import glob
import time

def ConvertImageForBug(src, src_w, src_h):
  dst = Image.new('RGB', (src_w, src_h))
  src_px = src.load()
  dst_px = dst.load()
  col = 0
  for x in range(src_w):
    if x in [12, 28, 44]:
      continue

    for y in range(src_h):
      if x>56:
        p = (0,0,0)
      else:
        p = src_px[col,y]
      dst_px[x,y] = p

    col += 1

  return dst


os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
#device_folder = glob.glob(base_dir + '28*')[0]
#device_file = device_folder + '/w1_slave'

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        #temp_f = temp_c * 9.0 / 5.0 + 32.0
        #return temp_c, temp_f
        return temp_c

# --- END TEMPERATURE READING CODE


if len(sys.argv) < 5:
    sys.exit("Usage: " + sys.argv[0] + " image brightness fade_in_ms wait_ms fade_out_ms [status] [name]")

image_file = sys.argv[1]
brightness = int(sys.argv[2])
fade_in_ms = int(sys.argv[3])
wait_ms = int(sys.argv[4])
fade_out_ms = int(sys.argv[5])

status = ""
if len(sys.argv) > 6:
  status = sys.argv[6]

rider_name = ""
if len(sys.argv) > 7:
  rider_name = sys.argv[7].decode('UTF-8')

print("Params:", image_file, brightness, fade_in_ms, wait_ms, fade_out_ms, status, rider_name)

brightness = max(3, min(100, brightness))

print("brightness that will be used:", brightness)


def msleep(value):
    time.sleep(value / 1000.0)
    
#temp = read_temp()
#temp = 0
#print("Temp:", temp)

# Show rider name
if rider_name:

  name = rider_name[0:10]

  # Work out which font size to fix all the text
  size = 25
  w = 100
  while (w > 54) and (size > 9):
    size -= 1
    font = ImageFont.truetype(fontfile, size)
    (w,h) = font.getsize(name)
      
  print("size:", size, "w:",w,"h:",h)

  image = Image.new('RGB', (60, 30))
  draw = ImageDraw.Draw(image)
  x = (60 - w) / 2 - 3;
  y = (30 - (h*1.2)) / 2;
  print("x:", x, "y:", y)
  draw.text((x,y), name, (255,255,255), font=font)
else:
  image = Image.open(image_file)

if status:
  man_image = Image.open("human.tga")
  image.paste(man_image, (0,0), man_image)

# Show temperature
#draw = ImageDraw.Draw(image)
#font = ImageFont.truetype(fontfile_bold, 10)
#draw.text((0,0), str(temp), (255,255,255), font=font)
#image.save("testimage.jpg")

# Configuration for the matrix
options = RGBMatrixOptions()
options.rows = 30 # Should be 30 but there is some weird scaling bug
options.cols = 60
options.chain_length = 1
options.parallel = 1
options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

matrix = RGBMatrix(options = options)

#max_brightness = matrix.brightness
#max_brightness = max_brightness / 4.0
max_brightness = brightness
matrix.brightness = max_brightness

print("max_brightness:", max_brightness)

# Make image fit our screen.
#image.thumbnail((matrix.width, matrix.height), Image.ANTIALIAS)

# Modify image as workaround for rendering bug in the LED library
image = ConvertImageForBug(image, options.cols, options.rows);

brightness_change_per_step = max(1, min(3, round(max_brightness / 10)))
steps_per_fade = max_brightness / brightness_change_per_step
fade_in_ms_per_step = fade_in_ms / steps_per_fade 
fade_out_ms_per_step = fade_out_ms / steps_per_fade 

print("brightness_change_per_step:", brightness_change_per_step)
print("steps_per_fade:", steps_per_fade)
print("fade_in_ms:", fade_in_ms)
print("fade_in_ms_per_step:", fade_in_ms_per_step)
print("fade_out_ms_per_step:", fade_out_ms_per_step)



try:
    print("Press CTRL-C to stop.")

    matrix.brightness = 0

    #matrix.SetImage(image.convert('RGB'))
    while (matrix.brightness < max_brightness):
        matrix.brightness = min(max_brightness, matrix.brightness + brightness_change_per_step)
        matrix.SetImage(image.convert('RGB'))
        msleep(fade_in_ms_per_step)
        
    msleep(wait_ms)

    while (matrix.brightness > 0):
        matrix.brightness = max(0, matrix.brightness - brightness_change_per_step)
        matrix.SetImage(image.convert('RGB'))
        msleep(fade_out_ms_per_step)
        
except KeyboardInterrupt:
    sys.exit(0)


