#!/bin/bash

cd `dirname $0`

brightness=50

in=1000
hold=5000
out=1000

sudo ./fade_in_out_viewer.py ridezoid_logo.tga $brightness $in $hold $out
sudo ./fade_in_out_viewer.py ridezoid_text.tga $brightness $in $hold $out

