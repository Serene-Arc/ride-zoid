#!/bin/bash

cd `dirname $0`

pwd


function read_buttons_and_name()
{
  export v1=`cat /tmp/blynk_v1`
  export v2=`cat /tmp/blynk_v2`
  export v3=`cat /tmp/blynk_v3`
  export name=`cat /tmp/blynk_name`
  export brightness=`cat /tmp/blynk_brightness`

  export status=""
  if [ "$name" != "" ]
  then 
    export status="booked"
  fi
}


in=200
hold=200
out=100

in=1000
hold=3000
out=1000

# initial brightness
brightness=25

while true
do

  read_buttons_and_name
  echo "v1=$v1 v2=$v2 v3=$v3 name=$name brightness=$brightness"

  if [ "$v1" = "1" ]
  then 
    sudo ./fade_in_out_viewer.py Uber1.tga $brightness $in $hold $out "$status"
    sudo ./fade_in_out_viewer.py Uber2.tga $brightness $in $hold $out "" "$name"
  fi

  read_buttons_and_name
  echo "v1=$v1 v2=$v2 v3=$v3 name=$name"

  if [ "$v2" = "1" ]
  then 
    sudo ./fade_in_out_viewer.py didi1.tga $brightness $in $hold $out "$status"
    sudo ./fade_in_out_viewer.py didi2.tga $brightness $in $hold $out "" "$name"
  fi

  read_buttons_and_name
  echo "v1=$v1 v2=$v2 v3=$v3 name=$name"

  if [ "$v3" = "1" ]
  then 
    sudo ./fade_in_out_viewer.py Ola1.tga $brightness $in $hold $out "$status"
    sudo ./fade_in_out_viewer.py Ola2.tga $brightness $in $hold $out "" "$name"
  fi

  if [ "$v1" != "1" ] && [ "$v2" != "1" ] && [ "$v3" != "1" ]
  then
    sudo ./fade_in_out_viewer.py $brightness ridezoid_logo.tga 100 1000 100
    sudo ./fade_in_out_viewer.py $brightness ridezoid_text.tga 100 1000 100
  fi

  sleep 1
done
