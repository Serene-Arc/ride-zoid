# ride-zoid

## Setup

First, clone the main repository. Then once this has finished, run the following command to clone the LED library: `git submodule update --init --recursive`. This is loaded as a submodule in the `libraries/' directory.

To build this LED library, navigate to `libraries/rpi-rgb-led-matrix/bindings/python/` and build the library. This can be done with the following commands:

1. `make build-python PYTHON=$(which python3)`
2. `make install-python PYTHON=$(which python3)`

Then, once this is installed, in the main ridezoid folder, use the command `sudo python3 -m pip install .` to install the module.

## Arguments

To run the script, go to the root directory (the directory with this README in it) and run the command `python3 -m ridezoid <arguments>`. The arguments in the program are included below:

- `image`
- `--brightness` has a default value of 25
- `--fade-in-ms` has a default of 1000
- `--wait-ms` has a default of 3000
- `--fade-out-ms` has a default of 1000
- `--status`
- `--name`
- `-v, --verbose`
- `--test` will write the output to the specified location instead of running the program through the GPIO

For example, `sudo python3 -m ridezoid --name 'Test Name'` to display that name. The `sudo` is required for a real run, but not using the `--test` flag. This is because the system access required to write to GPIO requires root access.
